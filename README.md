# PanDoc

PanDoc document generatie scripts.

## Genereratie van Documenten vanuit HUGO SSG Markdown

Generatie van diverse [PanDoc](https://pandoc.org) uitvoer-formaten vanuit één invoer-formaat op basis van Markdown die:

- Is opgezet als content voor de generatie van website publicaties via het [HUGO SSG-raamwerk](https://gohugo.io).
- Inclusief de dynamische `File tree` content opzet en extra ShortCodes in het gebruikte [HUGO Book Theme]()
- Aangevuld met maatwerk in het speciaal voor KIK-V<sup>2</sup> ontwikkelde [Book template](https://gitlab.com/kik-v2/shared/hugo-ssg/book-template)

Het daarvoor herbruikbare [HUGO to PanDoc script](.hugo-to-pandoc.yml) doorloopt de volgende stappen:

PM
